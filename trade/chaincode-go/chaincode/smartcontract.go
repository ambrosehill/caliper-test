/*
 * Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/*
* The sample smart contract for documentation topic:
* Writing Your First Blockchain Application
*/

package chaincode

/* Imports
* 4 utility libraries for formatting, handling bytes, reading and writing JSON, and string manipulation
* 2 specific Hyperledger Fabric specific libraries for Smart Contracts
*/
import (
    // "bytes"
    "encoding/json"
    "fmt"
    // "strconv"
    "github.com/hyperledger/fabric-contract-api-go/contractapi"

    // "github.com/hyperledger/fabric/core/chaincode/shim"
    // sc "github.com/hyperledger/fabric/protos/peer"
)

// SmartContract provides functions for managing an Asset
type SmartContract struct {
	contractapi.Contract
}

// Define the car structure, with 4 properties.  Structure tags are used by encoding/json library
type Trade struct {
    Uuid      string `json:"uuid"`
    Complete  bool   `json:"complete"`
    Asset     string `json:"owner"`
    Price     int    `json:"price"`
    Buyer     string `json:"buyer"`
    Seller    string `json:"seller"`
    BuyerQ    int    `json:"buyerQ"`
    SellerQ   int    `json:"sellerQ"`
}

type Asset struct {
    Uuid     string `json:"uuid"`
    Quality  int    `json:"quality"`
    Owner    string `json:"owner"`
}

/*
* The Init method is called when the Smart Contract "fabcar" is instantiated by the blockchain network
* Best practice is to have any Ledger initialization in separate function -- see initLedger()
*/
// InitLedger adds a base set of assets to the ledger
func (s *SmartContract) InitLedger(ctx contractapi.TransactionContextInterface) error {

	return nil
}
 
func (s *SmartContract) CreateTrade(ctx contractapi.TransactionContextInterface,assetID string, uuid string,buyer string, seller string) error {
 

    trade := Trade{
        Uuid: uuid,
        Complete: false,
        Asset:   assetID,
        Price:   100,
        Seller:  seller,
        Buyer:   buyer,
        SellerQ: -1,
        BuyerQ:  -1,
    }
    tradeAsBytes,_ := json.Marshal(trade)

    ctx.GetStub().PutState(uuid,tradeAsBytes)
    
    return nil
}


func (s *SmartContract) SubmitQuality(ctx contractapi.TransactionContextInterface, uuid string, quality int, submitter string) error {

    exists, err := s.AssetExists(ctx, uuid)
	if err != nil {
		return err
	}
	if !exists {
		return fmt.Errorf("the asset %s does not exist", uuid)
    }

    tradeAsBytes, err := ctx.GetStub().GetState(uuid)
    var trade Trade
    err = json.Unmarshal(tradeAsBytes, &trade)

    if(submitter == "buyer") {
        trade.BuyerQ = quality
    } else {
        trade.SellerQ = quality
    }

    if(trade.SellerQ == -1 || trade.BuyerQ == -1) {
        // Trade is over
    } else {
        if(trade.BuyerQ == trade.SellerQ) {
            // Finish trade
            trade.Complete = true
            assetAsBytes, err := ctx.GetStub().GetState(trade.Asset)
            if err != nil {
                return fmt.Errorf("failed to read from world state: %v", err)
            }
            var asset Asset
            err = json.Unmarshal(assetAsBytes, &asset)

            asset.Owner = "New Owner"

            assetAsBytes,_ = json.Marshal(asset)

            ctx.GetStub().PutState(trade.Asset,assetAsBytes)
            } else {
                // Call Mediator

            }     
    }
    tradeAsBytes, _ = json.Marshal(trade)    

    return ctx.GetStub().PutState(uuid, tradeAsBytes)
}

func (s *SmartContract) Oracle(ctx contractapi.TransactionContextInterface, uuid string, realQuality int) error {
    // Mediator has submitted the real quality. 
    tradeAsBytes, err := ctx.GetStub().GetState(uuid)
	if err != nil {
		return fmt.Errorf("failed to read from world state: %v", err)
	}
	if tradeAsBytes == nil {
		return fmt.Errorf("the asset %s does not exist", uuid)
    }

    var trade Trade

    err = json.Unmarshal(tradeAsBytes,&trade)

    trade.Complete = true
    
    // Check whose quality is correct
    assetAsBytes, err := ctx.GetStub().GetState(trade.Asset)
    if err != nil {
        return fmt.Errorf("failed to read from world state: %v", err)
    }
    var asset Asset
    err = json.Unmarshal(assetAsBytes, &asset)
    asset.Owner = trade.Buyer

    if(realQuality == trade.BuyerQ) {
        // Buyer is correct
        asset.Quality = realQuality
    } else if(realQuality == trade.SellerQ) {
        // Seller is correct
        asset.Quality = realQuality
    }

    return nil
}
 

func (s *SmartContract) ChangeOwner(ctx contractapi.TransactionContextInterface, uuid string, owner string) error {
    assetAsBytes, err := ctx.GetStub().GetState(uuid)
	if err != nil {
		return fmt.Errorf("failed to read from world state: %v", err)
	}
	if assetAsBytes == nil {
		return fmt.Errorf("the asset %s does not exist", uuid)
    }
    
	// log.Println(obsJSON)
	var asset Asset
    err = json.Unmarshal(assetAsBytes, &asset)
    
	if err != nil {
		return err
	}

    asset.Owner = owner

    assetAsBytes,_ = json.Marshal(asset)

	return ctx.GetStub().PutState(uuid,assetAsBytes)

}


func (s *SmartContract) ChangeQuality(ctx contractapi.TransactionContextInterface, uuid string, quality int) error {
    assetAsBytes, err := ctx.GetStub().GetState(uuid)
	if err != nil {
		return fmt.Errorf("failed to read from world state: %v", err)
	}
	if assetAsBytes == nil {
		return fmt.Errorf("the asset %s does not exist", uuid)
    }
    
	// log.Println(obsJSON)
	var asset Asset
    err = json.Unmarshal(assetAsBytes, &asset)
    
	if err != nil {
		return err
	}

    asset.Quality = quality

    assetAsBytes,_ = json.Marshal(asset)

	return ctx.GetStub().PutState(uuid,assetAsBytes)

}
func (s *SmartContract) DeleteAsset(ctx contractapi.TransactionContextInterface, uuid string) (error) {
    exists, err := s.AssetExists(ctx, uuid)
	if err != nil {
		return err
	}
	if !exists {
		return fmt.Errorf("the asset %s does not exist", uuid)
    }

    return ctx.GetStub().DelState(uuid)

}


func (s *SmartContract) ReadAsset(ctx contractapi.TransactionContextInterface, uuid string) (*Asset, error) {

    exists, err := s.AssetExists(ctx, uuid)
	if err != nil {
		return nil, err
	}
	if !exists {
		return nil, fmt.Errorf("the asset %s does not exist", uuid)
    }


    assetAsBytes, _ := ctx.GetStub().GetState(uuid)
    asset := Asset{}

    json.Unmarshal(assetAsBytes, &asset)

    return &asset,nil
}


// AssetExists returns true when asset with given ID exists in world state
func (s *SmartContract) AssetExists(ctx contractapi.TransactionContextInterface, id string) (bool, error) {
	assetJSON, err := ctx.GetStub().GetState(id)
	if err != nil {
		return false, fmt.Errorf("failed to read from world state: %v", err)
	}

	return assetJSON != nil, nil
}
