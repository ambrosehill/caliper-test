#!/bin/bash

echo "Running caliper tests... Passing flags"

iterations=$1
echo $1
for i in $( seq 1 $2 )
do
    node=`node --version`
    # Start the Network 
    pushd ../network
    ./network.sh createChannel
    ./network.sh deployCC
    popd
    sleep 10
    npx caliper launch manager --caliper-workspace ./ \
                               --caliper-networkconfig network/fabric-node-tls-solo.yaml \
                               --caliper-benchconfig benchmarks/benchmark-asset.yaml \
                               --caliper-bind-sut fabric:2.1 \
                               --caliper-flow-only-test \
                               --caliper-fabric-gateway-enabled  \
                               --caliper-report-path reports/report_asset_$i_$node.html
    # Stop the Network
    pushd ../network
    ./../network/network.sh down
    popd


    # npx caliper launch manager --caliper-workspace ./ \
    #                            --caliper-networkconfig network/fabric-node-tls-solo.yaml \
    #                            --caliper-benchconfig benchmarks/$1/benchmark-chunk.yaml \
    #                            --caliper-bind-sut fabric:2.1 \
    #                            --caliper-flow-only-test \
    #                            --caliper-fabric-gateway-usegateway  \
    #                            --caliper-fabric-loadbalancing \
    #                            --caliper-report-path reports/$1/report_chunk_$i.html
done