/*
 * Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/*
* The sample smart contract for documentation topic:
* Writing Your First Blockchain Application
*/

package chaincode

/* Imports
* 4 utility libraries for formatting, handling bytes, reading and writing JSON, and string manipulation
* 2 specific Hyperledger Fabric specific libraries for Smart Contracts
*/
import (
    // "bytes"
    "encoding/json"
    "fmt"
    // "strconv"
    "github.com/hyperledger/fabric-contract-api-go/contractapi"

    // "github.com/hyperledger/fabric/core/chaincode/shim"
    // sc "github.com/hyperledger/fabric/protos/peer"
)

// SmartContract provides functions for managing an Asset
type SmartContract struct {
	contractapi.Contract
}

// Define the car structure, with 4 properties.  Structure tags are used by encoding/json library
type Asset struct {
    Uuid     string `json:"uuid"`
    Quality  string `json:"quality"`
    Owner    string `json:"owner"`
}

/*
* The Init method is called when the Smart Contract "fabcar" is instantiated by the blockchain network
* Best practice is to have any Ledger initialization in separate function -- see initLedger()
*/
// InitLedger adds a base set of assets to the ledger
func (s *SmartContract) InitLedger(ctx contractapi.TransactionContextInterface) error {

	return nil
}
 
func (s *SmartContract) ChangeAsset(ctx contractapi.TransactionContextInterface,function string, uuid string, quality string, owner string) error {
 
    //  if len(args) != 1 {
    // 	 return shim.Error("Incorrect number of arguments. Expecting 1")
    //  }


    assetAsBytes,_ := ctx.GetStub().GetState(uuid)
    asset := Asset{}
    

    json.Unmarshal(assetAsBytes, &asset)

    if(function == "quality") {
        asset.Quality = quality
    } else if (function == "owner") {
        asset.Owner = owner
    } else if (function == "both") {

        asset.Quality = quality
        asset.Owner = owner
    }

    assetAsBytes,_ = json.Marshal(assetAsBytes)
    ctx.GetStub().PutState(uuid,assetAsBytes)
    
    return nil
}


func (s *SmartContract) CreateAsset(ctx contractapi.TransactionContextInterface, uuid string, owner string, quality string) error {
    
    
    assetJSON, err := ctx.GetStub().GetState(uuid)

    if assetJSON != nil {
        return fmt.Errorf("Asset Already exists")
    }
	if err != nil {
		return fmt.Errorf("failed to read from world state: %v", err)
    }
    
    asset := Asset{
        Uuid: uuid,
        Owner: owner,
        Quality: quality,
    }

    assetAsBytes, _ := json.Marshal(asset)    

    return ctx.GetStub().PutState(uuid, assetAsBytes)
}

func (s *SmartContract) CreateAsset2(ctx contractapi.TransactionContextInterface, uuid string, owner string, quality string) error {

    // exists, err := s.AssetExists(ctx, uuid)
	// if err != nil {
	// 	return err
	// }
	// if exists {
	// 	return fmt.Errorf("the asset %s already exists", uuid)
    // }

    assetJSON,_ := ctx.GetStub().GetState(uuid)
    
    if assetJSON == nil {
        return fmt.Errorf("Asset alreaady exists")
    }
    
    asset := Asset{
        Uuid: uuid,
        Owner: owner,
        Quality: quality,
    }

    assetAsBytes, _ := json.Marshal(asset)    

    return ctx.GetStub().PutState(uuid, assetAsBytes)
}


func (s *SmartContract) ChangeOwner(ctx contractapi.TransactionContextInterface, uuid string, owner string) error {
    assetAsBytes, err := ctx.GetStub().GetState(uuid)
	if err != nil {
		return fmt.Errorf("failed to read from world state: %v", err)
	}
	if assetAsBytes == nil {
		return fmt.Errorf("the asset %s does not exist", uuid)
    }
    
	// log.Println(obsJSON)
	var asset Asset
    err = json.Unmarshal(assetAsBytes, &asset)
    
	if err != nil {
		return err
	}

    asset.Owner = owner

    assetAsBytes,_ = json.Marshal(asset)

	return ctx.GetStub().PutState(uuid,assetAsBytes)

}


func (s *SmartContract) ChangeQuality(ctx contractapi.TransactionContextInterface, uuid string, quality string) error {
    assetAsBytes, err := ctx.GetStub().GetState(uuid)
	if err != nil {
		return fmt.Errorf("failed to read from world state: %v", err)
	}
	if assetAsBytes == nil {
		return fmt.Errorf("the asset %s does not exist", uuid)
    }
    
	// log.Println(obsJSON)
	var asset Asset
    err = json.Unmarshal(assetAsBytes, &asset)
    
	if err != nil {
		return err
	}

    asset.Quality = quality

    assetAsBytes,_ = json.Marshal(asset)

	return ctx.GetStub().PutState(uuid,assetAsBytes)

}
func (s *SmartContract) DeleteAsset(ctx contractapi.TransactionContextInterface, uuid string) (error) {
    exists, err := s.AssetExists(ctx, uuid)
	if err != nil {
		return err
	}
	if !exists {
		return fmt.Errorf("the asset %s does not exist", uuid)
    }

    return ctx.GetStub().DelState(uuid)

}


func (s *SmartContract) ReadAsset(ctx contractapi.TransactionContextInterface, uuid string) (*Asset, error) {

    exists, err := s.AssetExists(ctx, uuid)
	if err != nil {
		return nil, err
	}
	if !exists {
		return nil, fmt.Errorf("the asset %s does not exist", uuid)
    }


    assetAsBytes, _ := ctx.GetStub().GetState(uuid)
    asset := Asset{}

    json.Unmarshal(assetAsBytes, &asset)

    return &asset,nil
}


// AssetExists returns true when asset with given ID exists in world state
func (s *SmartContract) AssetExists(ctx contractapi.TransactionContextInterface, id string) (bool, error) {
	assetJSON, err := ctx.GetStub().GetState(id)
	if err != nil {
		return false, fmt.Errorf("failed to read from world state: %v", err)
	}

	return assetJSON != nil, nil
}
