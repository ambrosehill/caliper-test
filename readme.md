## Caliper Test 


This is a simple repo I set up to see if I can reproduce some of my bugs on other peoples machines. 



#### Pre-Reqs

1. Node (Please use version v8.13.0 or Let me know what version you used)
2. caliper v0.4.1
3. Hyperledger fabric binaries  (v2.1 I can send these to you if you cant get them easily). 
Place the binaries in a bin directory at the top level of the repo.


#### Steps

1. First install the dependeices above
```bash 
cd caliper
npm install
./run.sh 1
```